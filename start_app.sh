#!/bin/bash
install_path=/usr/share/arcuvc/
data_path=/userdata
ArcAICamera_path=${install_path%/}/ArcAICamera/

hwclock -s
cd $ArcAICamera_path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/oem/usr/lib

for i in $(seq 1 10)
do
        aicamera_id=$(ps -ef | grep aicamera | grep -v grep)
        if [ ! -n "$aicamera_id" ] ;then
            echo "Waitting RKAIQ Init ..."
            sleep 1
        else
            i=0
            break
        fi
done
if [ "$i" -eq "10" ];then
        echo "RKAIQ Init Err !!!"
        exit -1
fi
echo 77 > /sys/class/leds/PWM-IR/brightness
echo 0 > /sys/class/leds/PWM-RGB/brightness

sleep 3

env_path="config/environment.ini"
if [ -f $env_path ]; then
  static_resource_path=`cat $env_path | grep "StaticResourcePath=" |awk -F '=' '{print $2}'`
  echo $static_resource_path
  public_path=${static_resource_path}/public
  echo $public_path
  if [ ! -d  $public_path ];then
    echo "before prepare1"
    sh prepare.sh  $static_resource_path  & 
  fi
else
	if [ -f ${install_path}/initial_param.ini ];then
		mv ${install_path}/initial_param.ini $ArcAICamera_path
	fi	
    echo "before prepare2"
    sh prepare_data.sh
	if [ -n "$data_path" ];then
		mkdir -p $data_path
		sh prepare.sh $data_path &
	else
		sh prepare.sh &
	fi
fi 

sh ./start_proc.sh
exit 0
