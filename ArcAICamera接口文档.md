## 版本记录
|       版本        |    日期    | 更新说明               | SDK版本        | 备注 |
| :---------------: | :--------: | :--------------------- | ---------------------- | ---------------------- |
| 1.0.1.052013 | 05/20/2021 | 初始版本               | 1.0.22521010209.5 | 更新mac地址获取方式 |



## 一、接口使用说明

程序部署后，可以通过以下方式调用接口，

以连接设备为例，调用链接为`http://172.16.110.6/arm/connect`

## 二、WebServer接口

#### 1.连接设备 

**接口地址**

- 说明：连接设备，并设置时间
- 地址：`/arm/connect`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值               | 说明      |
| ---- | ------------ | ---------------- | --------- |
| 1    | Content-Type | application/json | JSON 格式 |

**请求体**

| 序号 | 键值      | 类型   | 说明         |
| ---- | --------- | ------ | ------------ |
| 1    | localTime | String | 设置设备时间 |

**请求体示例**

```json
{   
    "localTime":"2021-02-26 14:52:00" 
}
```

**返回值示例（成功）**

```json
{
    "code":200,
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```

 

#### 2.解绑设备

**接口地址**

- 说明：解绑设备
- 地址：`/arm/disconnect`
- 方法：`GET`

**返回值示例（成功）**

```json
{
    "code":200,
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 3.人员注册

**接口地址**

- 说明：人员注册，批量注册分多次调用该接口
- 地址：`/arm/registerPerson`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值                  | 说明           |
| ---- | ------------ | ------------------- | -------------- |
| 1    | Content-Type | multipart/form-data | form-data 格式 |

**请求体**

| 序号 | 键值         | 类型   | 说明                                                    |
| ---- | ------------ | ------ | ------------------------------------------------------- |
| 1    | personImg    | File   | 人员注册照 （大小限制 5K~3M 图片格式 jpg jpeg png bmp） |
| 2    | personName   | String | 人员姓名 （长度30）                                     |
| 3    | personNumber | String | 人员编号 （长度30，保证唯一标识）                       |



**返回值示例（成功）**

```json
{
    "code":200,
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg": "检测到多张人脸:196611"
}
```



#### 4.人员更新 

**接口地址**

- 说明：人员信息更新
- 地址：`/arm/updatePerson`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值                  | 说明           |
| ---- | ------------ | ------------------- | -------------- |
| 1    | Content-Type | multipart/form-data | form-data 格式 |

**请求体**

| 序号 | 键值         | 类型   | 说明                                                         |
| ---- | ------------ | ------ | ------------------------------------------------------------ |
| 1    | personImg    | File   | 人员注册照 （大小限制 5K~3M 图片格式 jpg jpeg png bmp）      |
| 2    | personName   | String | 人员姓名 （长度30）                                          |
| 3    | personNumber | String | 人员编号 （长度30，保证唯一标识）                            |
| 4    | updateImage  | Int    | 是否更新图片（1：是 ; 0：否 ,不更新注册照的情况下，可以不传 personImg ） |

**返回值示例（成功）**

```json
{
    "code":200,
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 5.人员删除 

**接口地址**

- 说明：人员删除
- 地址：`/arm/deletePerson`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值               | 说明      |
| ---- | ------------ | ---------------- | --------- |
| 1    | Content-Type | application/json | JSON 格式 |

**请求体**

| 序号 | 键值      | 类型      | 说明       |
| ---- | --------- | --------- | ---------- |
| 1    | personIds | JsonArray | 人员ID列表 |

**请求体示例**

```json
{   
    "personIds":[1,2,3] 
}
```

**返回值示例（成功）**

```json
{
    "code":200,
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 6.分页查询人员 

**接口地址**

- 说明：分页查询人员信息
- 地址：`/arm/pagePerson`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值               | 说明      |
| ---- | ------------ | ---------------- | --------- |
| 1    | Content-Type | application/json | JSON 格式 |

**请求体**

| 序号 | 键值        | 类型   | 说明                                         |
| ---- | ----------- | ------ | -------------------------------------------- |
| 1    | pageCurrent | Int    | 当前页                                       |
| 2    | pageSize    | Int    | 单页记录数                                   |
| 3    | query       | String | 查询条件（人员姓名或人员编号，支持模糊查询） |

**请求体示例**

```json
{   
    "pageCurrent":1,
    "pageSize":20,
    "query":""
}
```

**返回值**

| 序号 | 键值         | 类型   | 说明                                                         |
| ---- | ------------ | ------ | ------------------------------------------------------------ |
| 1    | pageCount    | Int    | 分页总数                                                     |
| 2    | pageCurrent  | Int    | 当前页                                                       |
| 3    | personId     | Int    | 人员ID                                                       |
| 4    | personImg    | String | 注册照路径(Nginx静态资源路径)，拼接ip以获取图片，示例`http://172.16.110.6/images/6913e776-0fae-4b85-a947-96a807af53bb.jpg` |
| 5    | personName   | String | 人员姓名                                                     |
| 6    | personNumber | String | 人员编号                                                     |
| 7    | totalCount   | Int    | 人员总数                                                     |

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "pageCount": 1,
        "pageCurrent": 1,
        "personList": [
            {
                "personId": 1,
                "personImg": "/images/6913e776-0fae-4b85-a947-96a807af53bb.jpg",
                "personName": "test",
                "personNumber": "123"
            }
        ],
        "totalCount": 1
    },
    "msg": "请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 7.根据personId查询人员 

**接口地址**

- 说明：查询某个人的详细信息
- 地址：`/arm/getPersonById`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值               | 说明      |
| ---- | ------------ | ---------------- | --------- |
| 1    | Content-Type | application/json | JSON 格式 |

**请求体**

| 序号 | 键值     | 类型 | 说明   |
| ---- | -------- | ---- | ------ |
| 1    | personId | Int  | 人员ID |

**请求体示例**

```json
{  
    "personId":1
}
```

**返回值**

| 序号 | 键值         | 类型   | 说明                                                         |
| ---- | ------------ | ------ | ------------------------------------------------------------ |
| 1    | personId     | Int    | 人员ID                                                       |
| 2    | personName   | String | 人员姓名                                                     |
| 3    | personImg    | String | 注册照路径(Nginx静态资源路径)拼接ip以获取图片，示例`http://172.16.110.6/images/7dcf4d6c-c1f5-46e2-831b-9831b6548a90.jpg` |
| 4    | personNumber | String | 人员编号                                                     |

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "personId": 1,
        "personImg": "/images/7dcf4d6c-c1f5-46e2-831b-9831b6548a90.jpg",
        "personName": "test",
        "personNumber": "123456"
    },
    "msg": "请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 8.获取识别参数

**接口地址**

- 说明：获取识别参数
- 地址：`/arm/getRecognizeSetting`
- 方法：`GET`

**返回值**

| 序号 | 键值                 | 类型  | 说明                  | 备注                      |
| ---- | -------------------- | ----- | --------------------- | ------------------------- |
| 1    | recogThreshold       | Float | 识别阈值              | [0,1],2位小数 0.8         |
| 2    | failRetryInterval    | Float | 识别失败重试间隔      | [1,10],1位小数            |
| 3    | successRetryStatus   | Int   | 识别成功重试间隔-开关 | 0:关;1:开                 |
| 4    | successRetryInterval | Float | 识别成功重试间隔-值   | [1,10],1位小数            |
| 5    | livenessMode         | Int   | 活体检测方式          | 0:关闭 1:rgb活体;2:ir活体 |
| 6    | irPreviewStatus      | Int   | IR小窗口              | 0:关;1:开                 |
| 7    | irLivenessThreshold  | Float | IR活体检测阈值        | [0,1],2位小数 默认0.7     |
| 8    | qualityThreshold     | Float | 质量检测阈值          | [0,1],2位小数 默认0.29    |
| 9    | rgbRotation          | Int   | rgb流旋转角度         | 只读，不允许设置          |
| 10   | irRotation           | Int   | ir流旋转角度          | 只读，不允许设置          |

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "failRetryInterval": 2,
        "irLivenessThreshold": 0.74,
        "irPreviewStatus": 0,
        "irRotation": 180,
        "livenessMode": 0,
        "qualityThreshold": 0.29,
        "recogThreshold": 0.78,
        "rgbRotation": 0,
        "successRetryInterval": 1,
        "successRetryStatus": 1
    },
    "msg": "请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 9.设置识别参数

**接口地址**

- 说明：设置识别参数
- 地址：`/arm/setRecognizeSetting`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值               | 说明      |
| ---- | ------------ | ---------------- | --------- |
| 1    | Content-Type | application/json | JSON 格式 |

**请求体**

| 类型                 | 说明                  | 类型  | 备注                      |
| :------------------- | :-------------------- | :---- | :------------------------ |
| recogThreshold       | 识别阈值              | Float | [0,1],2位小数 0.8         |
| failRetryInterval    | 识别失败重试间隔      | Float | [1,10],1位小数            |
| successRetryStatus   | 识别成功重试间隔-开关 | Int   | 0:关;1:开                 |
| successRetryInterval | 识别成功重试间隔-值   | Float | [1,10],1位小数            |
| livenessMode         | 活体检测方式          | Int   | 0:关闭 1:rgb活体;2:ir活体 |
| irPreviewStatus      | IR小窗口(暂时不支持)  | Int   | 0:关;1:开                 |
| irLivenessThreshold  | IR活体检测阈值        | Float | [0,1],2位小数 0.7         |
| qualityThreshold     | 质量检测阈值          | Float | [0,1],2位小数 0.29        |

**请求体示例**

```json
{
    "failRetryInterval":2,
    "irLivenessThreshold":0.7,
   	"irPreviewStatus":0,
    "livenessMode": 0,
    "qualityThreshold": 0.29,
    "recogThreshold": 0.8,
    "successRetryInterval": 2,
    "successRetryStatus": 1
}
```

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "failRetryInterval": 2,
        "irLivenessThreshold": 0.74,
        "irPreviewStatus": 0,
        "irRotation": 180,
        "livenessMode": 0,
        "qualityThreshold": 0.29,
        "recogThreshold": 0.78,
        "rgbRotation": 0,
        "successRetryInterval": 1,
        "successRetryStatus": 1
    },
    "msg": "请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 10.获取激活结果 

**接口地址**

- 说明：获取SDK激活结果
- 地址：`/arm/getActiveResult`
- 方法：`GET`

**返回值**

| 序号 | 键值         | 类型   | 说明      | 备注              |
| ---- | ------------ | ------ | --------- | ----------------- |
| 1    | activeStatus | Int    | 激活状态  | 0:未激活;1:已激活 |
| 2    | appId        | String | AppId     | 未激活状态下为空  |
| 3    | sdkKey       | String | SdkKey    | 未激活状态下为空  |
| 4    | activeKey    | String | ActiveKey | 未激活状态下为空  |

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "activeKey": "9851-343X-7245-4AT6",
        "activeStatus": 1,
        "appId": "D617np8jyKt4jN9gMr7ENbXzAmrykzWndGM8d68Ucafc",
        "sdkKey": "G1VJLUSqc2XnWfra39jcuL8Gf42Tei6AfoTJWDXkgDGx"
    },
    "msg": "请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 11.使用已有离线授权文件，再次激活 

**接口地址**

- 说明：如果本地已成功激活过，可以使用已有离线授权文件再次激活 
- 地址：`/arm/reactive`
- 方法：`GET`

**返回值示例（成功）**

```json
{
    "code":200,
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code": 400,
    "msg": "设备已激活:90114"
}
```



#### 12.采集设备信息 

**接口地址**

- 说明：采集设备指纹信息，上传到AI开放平台
- 地址：`/arm/collectDeviceInfo`
- 方法：`GET`

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": "Bxsjg7XQ3XPYzELp2xkya327XGH2gNomuiRO/SmGOG7FqiXtDeZd2naLbmwld3o9cMrSrKa295PYE7xZB35gZglu1SBFNznAwF8P6mkNp0+wkPcfaO+bToA7vAlWP0Iin3aoP+bVQgO94D6pzakuFgR+ROZJrl6YQJWrNWOi0vBOUXEJu7uhyPP4zYnldRhjLQCKw9qpc1lNCenB4Ded04De+EJdStW9meTNXKDizwIx2CV1sjWgvZXAAAHpMVDfEymR2i4B/oARBiEoqtteBFB9rcycEgb3y9PO+qDs3RiSnKXvoh/5ydYXi8qFXxEhPtnkG0IOBGWzeZb5Hir73Qv2pX08cO7Cpp19flS0yYaaaF9il8F+HFfbv9Tn0ZUjFnd2e6MwF9IVuLFMuHQ1NM97H82YYAjyzoQyRIDuOkQOph2LLDhzzqt781jOG+zRy22lGBO/ZU58eZTa5rAOjaR35vM2KeTGJOOckIYrVRU1o/sd6hZiTWmCQu8dWskPrF6/AwIifsoNIJGSoqiBAiHlxk8OqGeZ8GnuvZ3w6zrLqG9OQn9GkLTw0Q+usqxBvH2P517GK0HeAoEV+mdxK89NFyjPo41+BhcmOcqE6IgOXyW6sPPGpe5nAKxAMXP4b48UrHCLLeYgZaKj7Cha7OoO/oMPfuBrBYoEXcXQC/cryAQSDoYViFHf18igbfVFh1Dbuasasg9nK1nN1p9zJsJ2lWRIL4zXHHwwbdXzjAR2mz4Jao/EnDIQerxqZzf4Wcg99QbzW44Lgz/mr40CuxaOlSuh5kdoDSPb2sRbR80uOBYlV/HilrixUjrbP5ZUSGiYA/eYTkJWwhuUEF55PQ==",
    "msg": "请求成功"
}

```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```



#### 13.使用【离线授权文件】激活 

**接口地址**

- 说明：上传离线授权文件激活SDK
- 地址：`/arm/offlineActive`
- 方法：`POST`

**请求头**

| 序号 | 类型         | 值                  | 说明           |
| ---- | ------------ | ------------------- | -------------- |
| 1    | Content-Type | multipart/form-data | form-data 格式 |

**请求体**

| 序号 | 键值            | 类型 | 说明         |
| ---- | --------------- | ---- | ------------ |
| 1    | offlineAuthFile | File | 离线授权文件 |

**返回值**

| 序号 | 键值         | 类型   | 说明      | 备注              |
| ---- | ------------ | ------ | --------- | ----------------- |
| 1    | activeStatus | Int    | 激活状态  | 0:未激活;1:已激活 |
| 2    | appId        | String | AppId     | 未激活状态下为空  |
| 3    | sdkKey       | String | SdkKey    | 未激活状态下为空  |
| 4    | activeKey    | String | ActiveKey | 未激活状态下为空  |

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "activeKey": "9851-113X-7245-4AT6",
        "activeStatus": 1,
        "appId": "D617np8jyKt1jN9gMr7ENbXzAmrykzWndGM8d68Ucafc",
        "sdkKey": "G1VJLUSqc6XnWfra39jcuL8Gf42Tei6AfoTJWDXkgDGx"
    },
    "msg": "设备激活成功"
}
```

**返回值示例（失败）**

```json
{
    "code": 400,
    "msg": "离线激活失败:102407"
}
```



#### 14.分页查询识别记录

**接口地址**

- 说明：分页查询识别记录
- 地址：`/arm/pageDistinguishRecord`
- 方法：`POST`

**请求体**

| 序号 | 键值         | 类型      | 说明                                          |
| ---- | ------------ | --------- | --------------------------------------------- |
| 1    | pageCurrent  | Int       | 当前页                                        |
| 2    | pageSize     | Int       | 单页记录数                                    |
| 3    | query        | String    | 查询条件（人员姓名或人员编号，支持模糊查询）  |
| 4    | dateInterval | JsonArray | 查询时间范围                                  |
| 5    | pass         | String    | 识别结果（"":全部;"1":识别成功;"2":识别失败） |

**请求体示例**

```json
{     
	"pageCurrent": 1,     
	"pageSize": 1,    
	"query":"",    
	"dateInterval":["1970-01-01", "2030-01-01"],    
	"pass":"" 
}
```

**返回值示例（成功）**

```json
{
    "code": 200,
    "data": {
        "pageCount": 1,
        "pageCurrent": 1,
        "recordList": [
            {
                "createTime": "2021-05-26 16:45:37",
                "pass": "1",
                "personId": "6",
                "personName": "65654",
                "personNumber": "123456",
                "recognizeImage": "/recognizeImages/20210526/1622047537038.jpg",
                "similarity": 0.8065704703330994
            },
            {
                "createTime": "2021-05-26 16:45:30",
                "pass": "2",
                "personId": "-1",
                "personName": "-1",
                "personNumber": "-1",
                "recognizeImage": "/recognizeImages/20210526/1622047530009.jpg",
                "similarity": -1
            }
        ],
        "totalCount": 7
    },
    "msg": "请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```

**返回值**

| 序号 | 键值           | 类型   | 说明                                                         |
| ---- | -------------- | ------ | ------------------------------------------------------------ |
| 1    | personNumber   | String | 人员编号                                                     |
| 2    | personName     | String | 人员姓名                                                     |
| 3    | createTime     | Long   | 识别时间戳                                                   |
| 4    | personId       | Int    | 人员ID，也是PersonInfo表中的ID                               |
| 5    | pass           | String | 识别结果（"":全部;1":识别成功;"2":识别失败）                 |
| 6    | similarity     | Double | 相似度                                                       |
| 7    | recognizeImage | String | 识别照路径（Nginx静态资源路径），拼接ip以获取图片，示例`http://172.16.110.6/recognizeImages/20210526/1622047530009.jpg` |



#### 15.获取初始化结果

**接口地址**

- 说明：仅测试，无需接入，用于排查SDK异常的原因
- 地址：`/arm/getSDKInitResult`
- 方法：`GET`

**返回值示例（成功）**

```json
{
    "code":200.
    "data":90115,
    "msg":"请求成功"
}
```

#### 16.获取版本号

**接口地址**

- 说明：获取应用版本号
- 地址：`/arm/getVersion`
- 方法：`GET`

**返回值示例（成功）**

```json
{
    "code":200,
    "data":{
        "MainVersion":"1.0.1"
    }
    "msg":"请求成功"
}
```

**返回值示例（失败）**

```json
{
    "code":400,
    "msg":"请求失败"
}
```

## 三、Websocket接口

#### 1.Websocket 地址

[ws://172.16.110.6:8800/](ws://172.16.110.6:8800/)

设备ip地址 

#### 2.消息类型

每一帧检测结果

```json
{"rect":{"bottom":420,"left":391,"right":593,"top":218},"signalType":"FaceTract","trackId":12509}
```

开始检测

```json
{"signalType":"StartRecognize","trackId":12509}
```


识别中间结果提醒（提示信息）

```json
{"signalType":"FeedBack","trackId":12509,"type":1}
```

检测结果

```json
{"personId":76,"personImg":"/images/c9468bdd-3917-4b66-bf47-3d6129e4126c.jpg","personName":"test","personNumber":"1216-076","recogType":0,"rect":{"bottom":436,"left":405,"right":617,"top":224},"similarity":0.8867368102073669,"signalType":"RecognizeResult","trackId":12509}
```

