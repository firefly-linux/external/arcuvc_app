#!/bin/bash

original_setting_path=initial_param.ini

if [ ! -f $original_setting_path ]; then
	exit 1
fi

getValue(){
	param_=$1
	value_=`cat $original_setting_path | grep "$param_=" |awk -F '=' '{print $2}' | tr -d ' ' | tr -d '\r\n'`
	echo $value_
}


failRetryInterval=$(getValue failRetryInterval) 
irLivenessThreshold=$(getValue irLivenessThreshold) 
irPreviewStatus=$(getValue irPreviewStatus) 
livenessMode=$(getValue livenessMode) 
qualityThreshold=$(getValue qualityThreshold) 
recogThreshold=$(getValue recogThreshold) 
successRetryInterval=$(getValue successRetryInterval) 
successRetryStatus=$(getValue successRetryStatus) 
irRotation=$(getValue irRotation) 
rgbRotation=$(getValue rgbRotation) 


sqlite3 arcaicamera-backup.db << EOF

delete from SettingInfo;

update sqlite_sequence set seq ='0' where name ='SettingInfo';

insert into SettingInfo(tag,jsonDetail) values ("faceConfig",'{"failRetryInterval":$failRetryInterval,"irLivenessThreshold":$irLivenessThreshold,"irPreviewStatus":$irPreviewStatus,"livenessMode":$livenessMode,"qualityThreshold":$qualityThreshold,"recogThreshold":$recogThreshold,"successRetryInterval":$successRetryInterval,"successRetryStatus":$successRetryStatus,"irRotation":$irRotation,"rgbRotation":$rgbRotation}');

select * from SettingInfo;
EOF

rm -rf db/*
cp arcaicamera-backup.db db/arcaicamera.db