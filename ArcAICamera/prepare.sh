#!/bin/bash
if [ $# -eq 0 ];
then
	param_=$PWD # 全路�?
fi

if [ $# -eq 1 ];
then
	param_=$1 # 全路�?
fi

where_you_want_to_place=$param_

echo $where_you_want_to_place

if [ ${where_you_want_to_place: -1} != "/" ];
then
	where_you_want_to_place=${where_you_want_to_place}/
fi

echo "move public folder to " ${where_you_want_to_place}

tar -xvf ./tools/execs.tar -C /usr/bin/
chmod +x /usr/bin/treefrog
chmod +x /usr/bin/tadpole
chmod +x /usr/bin/tspawn

cp -rf public $where_you_want_to_place
chmod -R 777 ${where_you_want_to_place}public

if [ -f "/etc/nginx/nginx.conf.bak" ]; then
	rm -f /etc/nginx/nginx.conf.bak
fi 

mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
cp nginx.conf /etc/nginx/
sed -i  "s#static_resource_path#${where_you_want_to_place}public#g" /etc/nginx/nginx.conf

nginx_worker_id=`ps -ef|grep nginx|grep worker|awk '{print $2}'`
if [ -z "$nginx_worker_id" ];then
	nginx 
	sleep 2
fi

nginx -s reload

if [ -f "./config/environment.ini" ]; then
	rm -f ./config/environment.ini
fi 

cp -f  environment.ini config
sed -i  "s#static_resource_path#$where_you_want_to_place#g" config/environment.ini

tmp_dir=${where_you_want_to_place}tmp
mkdir -p $tmp_dir

if [ -f "./config/application.ini" ]; then
	rm -f ./config/application.ini
fi 

cp -f  application.ini config
sed -i  "s#tmp_path#$tmp_dir#g" config/application.ini

exit 0